import React from 'react';
import { render, fireEvent, waitFor } from '@testing-library/react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthScreen from '../templates/pages/AuthPage';

jest.mock('@react-native-async-storage/async-storage', () => ({
  setItem: jest.fn(),
  getItem: jest.fn(() => Promise.resolve('')),
}));

beforeEach(() => {
  jest.clearAllMocks();
});

describe('AuthScreen', () => {
  it('renders correctly', () => {
    const { getByPlaceholderText, getByText } = render(<AuthScreen navigation={{ navigate: jest.fn() }} />);
    
    expect(getByText('Авторизация')).toBeTruthy();
    expect(getByPlaceholderText('Лоигн')).toBeTruthy();
    expect(getByPlaceholderText('Пароль')).toBeTruthy();
    expect(getByText('Авторизоваться')).toBeTruthy();
  });

  it('updates login state on input change', () => {
    const { getByPlaceholderText } = render(<AuthScreen navigation={{ navigate: jest.fn() }} />);
    const loginInput = getByPlaceholderText('Лоигн');
    
    fireEvent.changeText(loginInput, 'testLogin');
    expect(loginInput.props.value).toBe('testLogin');
  });

  it('updates password state on input change', () => {
    const { getByPlaceholderText } = render(<AuthScreen navigation={{ navigate: jest.fn() }} />);
    const passwordInput = getByPlaceholderText('Пароль');
    
    fireEvent.changeText(passwordInput, 'testPassword');
    expect(passwordInput.props.value).toBe('testPassword');
  });

  it('handles authentication and navigation', async () => {
    const navigate = jest.fn();
    const { getByText, getByPlaceholderText } = render(<AuthScreen navigation={{ navigate }} />);
    
    fireEvent.changeText(getByPlaceholderText('Лоигн'), 'testLogin');
    fireEvent.changeText(getByPlaceholderText('Пароль'), 'testPassword');
    fireEvent.press(getByText('Авторизоваться'));

    await waitFor(() => {
      expect(AsyncStorage.setItem).toHaveBeenCalledWith('user', 'testLogin');
    }, { timeout: 3000 });

    await waitFor(() => {
      expect(AsyncStorage.setItem).toHaveBeenCalledWith('reg', '0');
    }, { timeout: 3000 });

    expect(navigate).toHaveBeenCalledWith('Расписание');
  });
});