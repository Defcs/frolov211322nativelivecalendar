import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

const sysImage = () => {
    return (
        <View style={styles.container}>
            <Image
                source={require('../../assets/Ioslogo.png')} 
                style={styles.image}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: 10,
    },
    image: {
        width: 50,
        height: 50,
    },
});

export default sysImage;