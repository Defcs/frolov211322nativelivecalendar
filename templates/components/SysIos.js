import React from 'react';
import { View, Text, Platform } from 'react-native';

const sysIos = () => {
    const deviceInfo = {
        OS: Platform.OS,
        Version: Platform.Version,
    };

    return (
        <View>
            <Text style={{ color: 'gray', textAlign: 'center' }}>Информация об устройстве iOS:</Text>
            <Text>ОС: {deviceInfo.OS}</Text>
            <Text>Версия: {deviceInfo.Version}</Text>
        </View>
    );
};

export default sysIos;