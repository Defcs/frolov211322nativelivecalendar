import React from 'react';
import { View, Text, Platform } from 'react-native';

const sysAndroid = () => {
    const deviceInfo = {
        OS: Platform.OS,
        Version: Platform.Version,
    };

    return (
        <View>
            <Text style={{ color: 'green', textAlign: 'center' }}>Информация об устройстве Android:</Text>
            <Text>ОС: {deviceInfo.OS}</Text>
            <Text>Версия: {deviceInfo.Version}</Text>
        </View>
    );
};

export default sysAndroid;