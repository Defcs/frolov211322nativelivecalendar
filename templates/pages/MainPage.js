import React, { useEffect, useState, useCallback } from 'react';
import { View, Text, Button, StyleSheet, FlatList, Modal, TextInput} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect } from '@react-navigation/native';
import json_response from '../../request_emulation/main_response.js';

function MainScreen({ navigation }) {
    const [data, setData] = useState([]);
    const [title, setTitle] = useState("");
    const [date, setDate] = useState("");
    const [desc, setDesc] = useState("");
    const [importance, setImportance] = useState("");
    const [isModalVisible, setModalVisible] = useState(false);


    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };

    const fetchDataFromStorage = async () => {
        try {
            const storedData = await AsyncStorage.getItem('data');
            console.log("storedData",storedData)
            if (storedData != []) {
                setData(JSON.parse(storedData));
                console.log("datadatadatadata",data)
            }
        } catch (error) {
            console.error('Я не получил данных из стора:', error);
        }
    };

    const usFun = async() =>{
            console.log("СРАБОТАД useEffect")
            setData(json_response.data);
            console.log("json_response.data",json_response.data)
            console.log("AsyncStorage.getItem('reg')",await AsyncStorage.getItem('reg'))
            if (await AsyncStorage.getItem('reg') == "0"){
                AsyncStorage.setItem('data',JSON.stringify(json_response.data));
                AsyncStorage.setItem('reg',"1");
            };
        }

    useEffect(() => {
        usFun();
        
        // async () => {
        //     await AsyncStorage.setItem('data',JSON.stringify(json_response.data));
        //     console.log("отработало",JSON.stringify(json_response.data))
        // };
    }, []);

    useFocusEffect(
        useCallback(() => {
            fetchDataFromStorage(); 
        }, [])
    );

    const renderItem = ({ item }) => (
        <View style={styles.itemContainer}>
            <Text style={styles.title}>Заголовок: {item.title}</Text>
            <Text>Дата: {item.date}</Text>
            <Text>Описание: {item.desc}</Text>
            <Text>Важность: {item.importance}</Text>
            <Button color="#ff0000" title="Удалить" onPress={() => handleDelete(item)} /> 
            {/* Стандартная кнопка оказалась ущербной к стилизации */}
            <Button title="Изменить" onPress={() => handleUpdate(item)} />
        </View>
    );

    const handleDelete = async (deletion) => {
        const newData = data.filter((item) => item !== deletion);
        setData(newData);
        await AsyncStorage.setItem('data', JSON.stringify(newData));
    };

    

    const handleUpdate = (itemToUpdate) => {
        console.log('обновил', itemToUpdate);
        navigation.navigate('Редактировать запись', {
            item: itemToUpdate,
        });
    };

    const handleAdd = () => {
        toggleModal();
    };

    const handleAddConfirm = (newItem) => {
        newItem={"id":data.length,"title":title,"date":date,"desc":desc,"importance":importance}
        setData([...data, newItem]);
        AsyncStorage.setItem('data', JSON.stringify([...data, newItem]));
        toggleModal();
    };

    return (
        <View style={styles.container}>
            <Text>Ваш календарь жизни</Text>
            <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={(item) => item.id}
            />
            <Button title="Добавить запись" onPress={() => handleAdd()} />
            <Modal 
                animationType="slide"
                transparent={true}
                visible={isModalVisible}
                onRequestClose={toggleModal}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.modalItem}>
                        <Text>Ведите новое расписание</Text>
                        <TextInput 
                            style={styles.input}
                            placeholder="Заголовок"
                            onChangeText={(text) => setTitle(text)}
                        />
                        <TextInput
                            style={styles.input}
                            placeholder="Дата"
                            onChangeText={(text) => setDate(text)}
                        />
                        <TextInput
                            style={styles.input}
                            placeholder="Описание"
                            onChangeText={(text) => setDesc(text)}
                        />
                        <TextInput
                            style={styles.input}
                            placeholder="Важность"
                            onChangeText={(text) => setImportance(text)}
                        />
                        <Button title="Добавить" onPress={handleAddConfirm} />
                        <Button title="Закрыть" onPress={toggleModal} />
                    </View>
                </View>
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // Ставим флекс, как истину, а значит можем применять свойства флекса
        justifyContent: 'center',
        // Размещаем все содержимое по центру
        alignItems: 'center',
    },
    modal:{
        // marginTop: 400,
        
    },
    modalContainer:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        
        // flex:1,
        // justifyContent:"center",
        // alignItems:"center",
    },
    modalItem:{
        width:300,
        height:300,
        padding:50,
        // alignItems:'center',
        // justifyContent: 'center',
        backgroundColor:"#fff",
    },
    itemContainer: {
        borderBottomWidth: 1,
        // Устанавливаю ширину нижней обводки на 1 px
        borderBottomColor: '#ccc',
        // Цвет нижней обводки серый
        padding: 10,
        // Внутреннии отступы со всех сторон по 10, чтобы не касалось краев экрана
        marginVertical: 5,
        // Внешний отступ сверху, чтобы побольше было и по красоте
    },
    title:{
        color:'#000',
        // Хочу выделить черным цветом
        fontSize:20,
        // ЧТобы видного было хочу 20 пикселей
        fontWeight:'800',
        // Жирный заголовок
    }
});

export default MainScreen;