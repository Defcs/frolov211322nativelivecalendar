import React, { useMemo, useState, useRef, useEffect} from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import { Platform } from 'react-native';
import SysAndroid from '../components/SysAndroid'; 
import SysIos from '../components/SysIos'; 
import SysImage from '../components/SysImage';
// import { green } from 'react-native-reanimated/lib/typescript/reanimated2/Colors';

function MoreInfScreen(){
    const intervalRef = useRef()
    const [count, setCount] = useState(0)
    
    const platformMessage = Platform.OS === 'android' 
    ? { text: 'Вы пользуетесь системой "Андроид"', color: 'green' } 
    : { text: 'Похоже, что Ваша система отлична от "Андройд"', color: 'gray' };

    useEffect(() => {
        intervalRef.current = setInterval(
            () => setCount((count) => count + 1),
            1000
        )
    
        return () => {
            clearInterval(intervalRef.current)
        }
    }, [])
    
    let memoInf
    function Calc() {
        return 'Тут результат вычислений какой-то сложной функции.(Но так как сложной функции пока и нет,то это только кейс попытки использования хеша калк)'
    };

    const DeviceInfo = Platform.select({
        android: <SysAndroid />,
        ios: <SysIos />,
        default: (
            <View>
                <Text style={{ color: 'yellow', textAlign: 'center' }}>Вероятно, вы пользуетесь вебсайтом или неизвестной системой.</Text>
            </View>
        ),
    });

    const result = useMemo(() => Calc(), [memoInf]);
    return(
        <View>
            <Text  style={styles.textImportantStyle}>{result}</Text>
            <Text>Сколько времени проведено в моей мобильном приложении, после первого посещения данной страницы.</Text>
            <Text style={{ fontSize: 120,textAlign:"center" }}>{count}</Text>
            {/* размер шрифта 120, чтобы были круаные цифры */}
            <Button style={styles.buttonStyle}
                title="Остановить"
                onPress={() => {
                clearInterval(intervalRef.current)
                }}
            />
            <Text style={[styles.platformText, { color: platformMessage.color }]}>
                {platformMessage.text}
            </Text>
            <Text style={styles.platformTitle}>Больше информации о платформе.</Text>
            {DeviceInfo}
            <SysImage />
        </View>
    )
    
}
const styles = StyleSheet.create({
    platformTitle: {
        fontSize: 20,
        textAlign:"center",
        ...Platform.select({
            ios:{
                color:'gray',
            },
            android:{
                color:'green',
            },
            default:{
                color:'yellow',
            }
        })
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 20,
    },
    buttonStyle: {
        width: '50%',
        backgroundColor: "#000",
    },
    textImportantStyle: {
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center",
    },
    platformText: {
        fontSize: 18,
        fontWeight: 'bold',
    },
})


export default MoreInfScreen