import React, { useState } from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
// import { MainContext } from '../../App.js'

function AuthScreen({ navigation }) {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    // const contextValue = useContext(MainContext);

    const storeAuth = async (value1) => {
        try {
            
            await AsyncStorage.setItem('user', value1);
            await AsyncStorage.setItem('reg', "0");
            console.log("reg был установлен",await AsyncStorage.getItem('reg'), await AsyncStorage.getItem('user'))
        } catch (e) {
            console.log('Ошибка при сохранении пользователя:', e);
        }
    };

    const handleAuth = async () => {
        console.log('handleAuth called');
        await storeAuth(login);
        navigation.navigate('Расписание');
    };

    
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <View style={styles.formWrapperSec}>
                <View style={styles.formWrapper}>
                    {/* флекс и все содержимое посередине */}
                    <Text testID="authTitle">Авторизация</Text>
                    {/* <Text>{  contextValue }</Text> */}
                        <TextInput
                            placeholder="Лоигн"
                            value={login}
                            onChangeText={setLogin}
                            style={{width:"80%", height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 10, paddingHorizontal: 8, marginTop:10 }}
                        // тоже самое, не стал выносить в отдельный стилистический блок, стилей мало
                        />
                    <TextInput
                        placeholder="Пароль"
                        value={password}
                        onChangeText={setPassword}
                        secureTextEntry
                        style={{width:"80%", height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 10, paddingHorizontal: 8 }}
                        // Стили: высота 40, цвет серый, ширина оьводки 1, отступ внешний вниз 10, отступы слева и справа изнутри по 8 пикселей
                    />
                    <Button testID="authButton" title="Авторизоваться" onPress={handleAuth} />
                </View>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    formWrapperSec: {
        height:200,
        width:"100%",

    },
    formWrapper: {
        width:"80%",
        marginLeft:"auto",
        marginRight:"auto",
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        borderColor: 'black', 
        borderWidth: 5
    }
})
export default AuthScreen;