import React, { useEffect, useState, useCallback } from 'react';
import { View, Text, Button ,Image, StyleSheet, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Asset } from 'expo-asset';
import * as ImagePicker from 'expo-image-picker';

function UserScreen({ navigation }) {
    const [userData, setUserData] = useState(null);
    const default_img = Asset.fromModule(require('../../assets/default_user.png')).uri;
    const [photo, setPhoto] = useState({"uri":default_img});

    const fetchUserData = useCallback(async () => {
        try {
            const storedUserData = await AsyncStorage.getItem('user');
            console.log('storedUserData', storedUserData);
            if (storedUserData) {
                setUserData(storedUserData);
            } else {
                console.log('Данных нет');
                navigation.navigate('Главная');
            }
        } catch (error) {
            console.error('Ошибка получения пользователя:', error);
        }
    }, []);

    const handleChoosePhoto = async () => {
        try {
          const result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
          });
    
          if (!result.cancelled) {
            setPhoto(result.assets[0]);
          }
        } catch (error) {
          console.log('Ошибка выбора фото:', error);
        }
      };

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', fetchUserData);
        return unsubscribe;
    }, [navigation, fetchUserData]);

    const handleDeletePhoto = () =>{
        setPhoto({"uri":default_img});
    }

    const handleLogout = async () => {
        try {
            await AsyncStorage.removeItem('user');
            setUserData(null);
            navigation.navigate('Главная');
        } catch (error) {
            console.error('Какая-то ошибка при удалении айтома:', error);
        }
    };

    if (userData === null) {
        console.log('navigate');
        navigation.navigate('Главная');
    }

    return (
        <View style={styles.bodyStyle}>
            <Text style={styles.textVeryImportantStyle}>Страница пользователя</Text>
            {userData && <Text  style={styles.textImportantStyle}>Ваш логин: {userData}</Text>}
            {userData && <Text style={styles.textImportantStyle}>А это Вы:</Text>}
            {photo && <Image source={{ uri: photo.uri }} style={styles.imgStyle} />}
            <View style={styles.buttonWrapper}>
                <TouchableOpacity   onPress={handleChoosePhoto} style={styles.TouchableOpacityStyle}><Text>Изменить фото</Text></TouchableOpacity>
                <TouchableOpacity   onPress={handleDeletePhoto} style={styles.TouchableOpacityStyle}><Text>Удалить фото"</Text></TouchableOpacity>
            </View>
            <Button title="Выйти из аккаунта" onPress={handleLogout} />
        </View>
    );
}

const styles = StyleSheet.create({
    TouchableOpacityStyle: {
        width: "47%",
        height: "40%",
        backgroundColor: "lightblue",
        alignItems: "center", 
        justifyContent: "center", 
        margin: 5,
    },
    buttonWrapper: {
        flexDirection: 'row', 
    },
    imgStyle: {
        marginLeft:"auto",
        marginRight:"auto",
        marginTop:30,
        marginBottom:30,
        width:200,
        height:200,
    },
    bodyStyle:{
        marginTop:100,
    },
    textImportantStyle: {
        fontSize:20,
        fontWeight:"bold",
    },
    textVeryImportantStyle: {
        fontSize:30,
        fontWeight:"bold",
        color:"blue",
        textAlign:"center",
    }
})
export default UserScreen;