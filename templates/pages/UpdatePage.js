import React, { useReducer } from 'react';
import { View, Text, Button, TextInput, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const reducer = (state, action, id = 0) => {
    switch (action.type) {
    case 'SET_TITLE':
        console.log("set title",state[id] )
        console.log("hehe",state)
        console.log("{ ...state, title: action.payload }",{ ...state})
        return { ...state, title: action.payload };
    case 'SET_DATE':
        return { ...state, date: action.payload };
    case 'SET_DESC':
        return { ...state, desc: action.payload };
    case 'SET_IMPORTANCE':
        return { ...state, importance: action.payload };
    default:
        return state;
    }
};

function UpdateScreen({ route, navigation }) {
    // console.log("FFFFF")
    const { item } = route.params;
    const [state, dispatch] = useReducer(reducer, item);

    const handleUpdate = async () => {
        data = JSON.parse(await AsyncStorage.getItem('data'))
        data[item.id] = state
        console.log("state_item",state)
        await AsyncStorage.setItem('data',JSON.stringify(data))
        navigation.navigate('Расписание');
    };

    return (
        <View style={styles.container}>
            <Text>Изменить запись</Text>
            <TextInput
                style={styles.input}
                placeholder="Заголовок"
                value={state.title}
                onChangeText={(text) => dispatch({ type: 'SET_TITLE', payload: text },item.id)}
            />
            <TextInput
                style={styles.input}
                placeholder="Дата"
                value={state.date}
                onChangeText={(text) => dispatch({ type: 'SET_DATE', payload: text })}
            />
            <TextInput
                style={styles.input}
                placeholder="Описание"
                value={state.desc}
                onChangeText={(text) => dispatch({ type: 'SET_DESC', payload: text })}
            />
            <TextInput
                style={styles.input}
                placeholder="Важность"
                value={state.importance}
                onChangeText={(text) => dispatch({ type: 'SET_IMPORTANCE', payload: text })}
            />
            <Button title="Обновить" onPress={handleUpdate} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 10,
        padding: 10,
        width: '80%',
    },
});

export default UpdateScreen;