import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AuthScreen from './templates/pages/AuthPage';
import MainScreen from './templates/pages/MainPage';
import UserScreen from './templates/pages/UserPage';
import UpdateScreen from './templates/pages/UpdatePage';
import MoreInfScreen from './templates/pages/MoreInf';
import 'react-native-reanimated';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const MainApp = () => {
  return (
    <Stack.Navigator initialRouteName="Страница авторизации">
      <Stack.Screen name="Страница авторизации" component={AuthScreen} />
      <Stack.Screen name="Расписание" component={MainScreen} />
      <Stack.Screen name="Редактировать запись" component={UpdateScreen} />
    </Stack.Navigator>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Главная" component={MainApp} />
        <Tab.Screen name="Аккаунт" component={UserScreen} />
        <Tab.Screen name="Интересная информация" component={MoreInfScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;